<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->


<html>
    <head>
        <title>TP Inteligencia Artificial N°2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        <div div class="container">
            <div div class="row">
                <div div class="col-md-12">
                    <h1>TP Inteligencia Artificial N°2</h1>
                </div>
                <div class="col-md-6">
                    <div class="controles">
                        <form action="index.php" method="get">
                            Alto mapa
                            <input id="plaza" name="alto" value="" type="number"/>
                            <hr>
                            Ancho mapa
                            <input id="plaza" name="ancho" type="number"/>
                            <hr>
                            Plaza (1-10)
                            <input id="plaza" name="plaza" type="number"/>
                            <hr>
                            Comercio (1-10)
                            <input id="comercio" name="comercio" type="number"/>
                            <hr>
                            Seguridad (1-10)
                            <input id="seguridad" name="seguridad" type="number"/>
                            <hr>
                            <input type="submit" value="Calcular">
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                        <?php 
                        $alto = $_GET['alto'];
                        $ancho = $_GET['ancho'];
                        $plaza = $_GET['plaza'];
                        ?>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d22741.9582847919!2d-58.46499593430776!3d-34.594731490435!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sar!4v1540068697065" width="<?php echo $ancho*10; ?>" height="<?php echo $alto*10; ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <div id="mapa">
                        <?php 
                        
                            for($i = 0; $i < $alto; $i++) {
                                for($j = 0; $j < $ancho; $j++) {
                                    echo'<div x="'.$j.'" y="'.$i.'" class="block chartreuse"></div>';
                                }
                            }

                        ?>

                    </div>
                </div>
            </div>
        </div>
        
        <script>

           
            var poblacion = [];
            var matrix = [];
            var resultado =[];
            var caminoN = 0;
            
            matrix = init_matrix(<?php echo $alto; ?>,<?php echo $ancho; ?>);
            // Poblacion Cantidad Individuos, Largo Camino
            poblacion = poblacionInicial(10000,100,matrix)
            
            draw(matrix);
            
            
            
            function start(){
                
                reset_matrix(matrix);
                resultado = cruzar(poblacion);
                for(var i=0; i< 10000; i++){
                    // Cruzar, Poblacion
                    resultado = cruzar(resultado);
                    // Mutacion, Probabilidad, Cantidad de Genes
                    resultado = mutacion(resultado,1,10);
                    resultado = aptitud(resultado,95);
                    if(resultado.length == 1){
                        break;
                    }
                    console.log("Iteracion:"+i);
                    console.log("Poblacion:"+resultado.length)
                }
                draw_camino(resultado[0],matrix);

                //for(var i = 0; i < poblacion.length ; i++){
                //    draw_camino(poblacion[i],matrix);
                //}
                //setTimeout(function(){
                //    start()
                //},500)
            }
            
            
            start();
            

            $(document).keydown(function(e) {
                    if(e.which == 39){
                        caminoN++;
                        //reset_matrix(matrix);
                        draw_camino(resultado[caminoN],matrix);
                    }
                    if(e.which == 37){
                        caminoN--;
                        //reset_matrix(matrix);
                        draw_camino(resultado[caminoN],matrix);
                    }
            });
            
            function aptitud(poblacion_,obtener){
                var poblacion = poblacion_;
                var result = [];
                var score = 0;
                var avg = 0;
                for(var i=0; i< poblacion.length; i++){
                        score = diferenciaCero(poblacion[i]);
                        avg += score;
                        result.push({score: score, item: poblacion[i]});
                }
                
                result.sort(compare);
                var tope = Math.floor((result.length*obtener)/100)
                console.log("Puntaje iteracion: "+avg/i+"%");
                console.log("Poblacion total: "+result.length);
                console.log("Porcentaje deseado: "+obtener+"%");
                console.log("Poblacion que queda: "+tope);
                result =  result.slice(0,tope);
                var result_ = [];
                for(var i=0; i< result.length; i++){
                    result_.push(result[i].item);
                }
                return result_;
            }
            function compare(a,b) {
              if (a.score < b.score)
                return -1;
              if (a.score > b.score)
                return 1;
              return 0;
            }
            
            
            
            function diferenciaCero(camino){
                var x1 = -200;
                var y1 = -200;
                var pos_y = 0;
                var pos_x = 0;
                for(var i=0; i<camino.length; i++) {
                    if(camino[i].mov == 'A'){
                        pos_y--; 
                    }
                    else if(camino[i].mov == 'B'){
                        pos_y++; 
                        
                    }
                    else if(camino[i].mov == 'I'){
                        pos_x--; 
                        
                    }
                    else if(camino[i].mov == 'D'){
                        pos_x++; 
                        
                    }
                }
                var a = x1 - pos_x;
                var b = y1 - pos_y;
                
                return Math.sqrt( a*a + b*b );
                
            }
            
            function muchaIzquierda(individuo){
                var nice = 0;
                for(var i=0; i< individuo.length; i++){
                    if(individuo[i].mov == 'I'){
                        nice++;
                    }
                }
                return parseFloat(((nice*100)/ parseInt(individuo.length)));
            }
            function indexOfMax(arr) {
                if (arr.length === 0) {
                    return -1;
                }

                var max = arr[0];
                var maxIndex = 0;

                for (var i = 1; i < arr.length; i++) {
                    if (arr[i] > max) {
                        maxIndex = i;
                        max = arr[i];
                    }
                }

                return maxIndex;
            }
            function cruzar(poblacion_){
                var result =[];
                var poblacion = poblacion_;
                for(var i=0; i< poblacion.length; i++){
                        var nuevo_individuo = [];
                        var rand = Math.floor(Math.random()*(poblacion.length-1));
                        individuo_viejo1 = poblacion[i];
                        individuo_viejo2 = poblacion[rand];
                        for(var j = 0; j < poblacion[0].length; j++){
                            if(j%2 == 0){
                                nuevo_individuo.push(individuo_viejo1[j]);
                            }
                            else{
                                nuevo_individuo.push(individuo_viejo2[j]);
                            }
                        }
                        result.push(nuevo_individuo);
                    }
                return result;
            }
            
            function mutacion(poblacion_,coef,genes){
                var poblacion = poblacion_;
                for(var i=0; i< poblacion.length; i++){
                    if(Math.floor(Math.random()*100) <= coef){
                        var color = randomColor();
                        for(var j=0; j< genes; j++){
                            poblacion[i][Math.floor(Math.random()*(poblacion[i].length-1))] = {mov:randomMov(),color:color}
                        }
                    }
                }
                return poblacion;
            }
            
            function randomMov(){
                 var rand = Math.floor(Math.random()*4);
                   if(rand == 0){
                       return 'A';
                   }
                   else if(rand == 1){
                       return 'B';
                   }
                   else if(rand == 2 ){
                       return 'I';
                   }
                   else if(rand == 3){
                       return 'D';
                   }
            }
            function shuffle(a) {
                var j, x, i;
                for (i = a.length - 1; i > 0; i--) {
                    j = Math.floor(Math.random() * (i + 1));
                    x = a[i];
                    a[i] = a[j];
                    a[j] = x;
                }
                return a;
            }
            
            function poblacionInicial(cantidad,largo_camino,mapa){
                var poblacion =[];
                for(var i=0; i< cantidad ; i++){
                    poblacion.push(crear_camino(largo_camino,mapa))
                }
                return poblacion;
            }
            function randomColor(){
                return "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
            }
    
            function crear_camino(largo,matriz2){
                var camino = [];
                var matriz = [];
                var antx = 0;
                var anty = 0;

                var color = randomColor();
              
                for(var i=0; i< largo; i++){
                   var rand = Math.floor(Math.random()*4);
                   if(rand == 0){
                       anty--;
                       if(anty >= 0){
                            camino.push({mov:'A',color:color});
                       }
                       else{
                           i--;
                           anty++;
                       }
                   }
                   else if(rand == 1){
                       anty++;
                       if(anty <= matriz2.length){
                           camino.push({mov:'B',color:color});
                       }
                       else{
                           i--;
                           anty--;
                       }
                   }
                   else if(rand == 2 ){
                       antx--;
                       if(antx >= 0){
                           camino.push({mov:'I',color:color});
                       }
                       else{
                           i--;
                           antx++;
                       }
                   }
                   else if(rand == 3){
                       antx++;
                       if(antx <= matriz2[0].length ){
                           camino.push({mov:'D',color:color});
                       }
                       else{
                           i--;
                           antx--;
                       }
                   }
                }
         
                return camino;
                
            }        
           
            function init_matrix(alto,ancho){
                var matrix = [];
                for(var i=0; i<alto; i++) {
                    matrix[i] = [];
                    for(var j=0; j<ancho; j++) {
                        matrix[i][j] = crear_pixel();
                    }
                }
                return matrix;
            }
            
            function crear_pixel(){
                var pixel;
                pixel = {"plaza": Math.floor(Math.random()*10),
                         "comercio": Math.floor(Math.random()*10),
                         "seguridad": Math.floor(Math.random()*10),
                         "color": "chartreuse",
                        };
                return pixel;
            }
            
            function draw_camino(camino, matrix){
                var ancho = matrix.length;
                var alto = matrix[0].length;
                var pos_x = Math.floor(matrix.length/2);
                var pos_y = Math.floor(matrix[0].length/2);
                $("#mapa .block[x='"+matrix.length/2+"'][y='"+Math.floor(matrix[0].length/2)+"']").css("background-color",camino[0].color);
                for(var i=0; i<camino.length; i++) {
                    if(camino[i].mov == 'A'){
                        pos_y--; 
                    }
                    else if(camino[i].mov == 'B'){
                        pos_y++; 
                        
                    }
                    else if(camino[i].mov == 'I'){
                        pos_x--; 
                        
                    }
                    else if(camino[i].mov == 'D'){
                        pos_x++; 
                        
                    }
                    $("#mapa .block[x='"+pos_x+"'][y='"+pos_y+"']").addClass("run");
                    $("#mapa .block[x='"+pos_x+"'][y='"+pos_y+"']").css("background-color",camino[i].color);
                }
            }
            
            function reset_matrix(matrix){
                for(var i=0; i<matrix.length; i++) {
                    for(var j=0; j<matrix[0].length; j++) {
                        $("#mapa .block").removeClass("run");
                        $("#mapa .block[x='"+j+"'][y='"+i+"']").css("background-color",'chartreuse');

                    }
                }
                
            }
            
            function draw(matrix){
                
                $("#mapa").css("width",matrix.length*10+"px");
                $("#mapa").css("height",matrix[0].length*10+"px");
            }
            
            
        </script>
        <style>
            #mapa{
                position: absolute;
                top: 0px;
            }
            .block{
                border: 0px;
                width: 10px;
                height: 10px;
                display: block;
                float:left;
            }
            .chartreuse{
                background-color: #39FF00;
                opacity: 0.3;
            }
            .run{
                opacity: 1;
            }
            .red{
                background-color: red;
            }
        </style>
    </body>
</html>
